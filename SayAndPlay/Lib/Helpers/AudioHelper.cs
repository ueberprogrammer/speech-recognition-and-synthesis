﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Speech.AudioFormat;
using System.Speech.Synthesis;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Lib.Helpers
{
    public static class AudioHelper
    {
        const string iamToken = "CggVAgAAABoBMxKABLT_3RdGnPTTS49VaX_ZAM4li5KNyADTDQ5zms0n0PWuCPxuQQA8fg6SCrNAzs3SOeeFomgMQhqt6ml68At7KILU5v4d8YFNEq6PI_7YaPcYBxS4ma_YkS4upsRKTZbPCu7TrSdWQ2qp6sxzUiJ2b8926OEFX94Xo13tFWPDOm9CLbpVQbpomJD7EfdD1cqR5A3hO38DeRHh-GtNTCsJ-Wgxze13QZl8Uc_-N0RE9bLfkU5qh-Eju7uEi6AQT5vYWR9FAdqZfF1cim7LHlrtfr3_iPJhGNLeaCCXwB9mPZqnyC7elKzwhkHQkUtsJVE_WxoLV3AUMGHzV_HuXHajcxGHw2UDiRUz1-i2jaPtHmeNjjxW2WPRU59d82PlABadRXkZSc7d4-cabQZQ7mA5nyRMENm57U9ATgBKJNkfyR20wVMjqG8N_VipjrAs4WmTZjvXH8ATCEumKkefV2kYz_XjqatO0NI_PQyJkztRuRzwA--FqI0Nr2JTEV6-BpejDS7JwdkDrsBA9QIavSEd2OdHWPaaIrJl4Mbaa6k9UHsBF9owODTdk5dgyNqBlIBEtE_akkJTvR-emMLQ-TeodWYKephMu-gsEt9Hv9STdDRjjOfJCAZFZdyKFFAIK32JagE80UwZrsvbbMWAwcYis_Wa_PutAR1ibkFic-JmuLKDGiQQ4vSr7wUYosau7wUiFgoUYWplNWJtazQwZXMwN2NoNjQ3MjE="; // Укажите IAM-токен.

        public static async Task<string> RecognizeAsync(string fileName)
        {
            const string folderId = "b1gtgbgn9s7i2cvuc1m6"; // Укажите ID каталога.
            const string lang = "ru-RU";
            const string topic = "maps";//"general";

            var url = $"https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?topic=general&folderId={folderId}&lang={lang}&topic={topic}";

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + iamToken);

            var bytes = File.ReadAllBytes(fileName);

            var byteContent = new ByteArrayContent(bytes);
            var response = await client.PostAsync(url, byteContent);

            var jsonString = await response.Content.ReadAsStringAsync();

             var recoginzeResponse = JsonConvert.DeserializeObject<RecoginzeResponse>(jsonString);

            return recoginzeResponse.Result;
        }

        public static async Task<string> RecognizeV2Async(Stream stream)
        {
            const string folderId = "b1gtgbgn9s7i2cvuc1m6"; // Укажите ID каталога.
            const string lang = "ru-RU";
            const string topic = "maps";//"general";
            var format = "lpcm";
            var sampleRateHertz = "48000";

            var url = $"https://stt.api.cloud.yandex.net/speech/v1/stt:recognize?topic={topic}&folderId={folderId}&format={format}&sampleRateHertz={sampleRateHertz}&lang={lang}";

            var client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + iamToken);

            var bytes = ReadFully(stream);

            var byteContent = new ByteArrayContent(bytes);
            var response = await client.PostAsync(url, byteContent);

            var jsonString = await response.Content.ReadAsStringAsync();

            var recoginzeResponse = JsonConvert.DeserializeObject<RecoginzeResponse>(jsonString);

            return recoginzeResponse.Result;
        }

        private static bool b = true;

        public static async Task<string> SynthesizeAsync(string fileName, string text)
        {
            
            var newFileName = b ? await SynthesizeByMicrosoftAsync(fileName, text)
                                : await SynthesizeByApiAsync(fileName, text);

            b = !b;

            return newFileName;
        }

        private static async Task<string> SynthesizeByMicrosoftAsync(string fileName, string text)
        {
            var task = Task.Run(() =>
            {
                using (var synth = new SpeechSynthesizer())
                {
                    //synth.SelectVoice("IVONA 2 Tatyana OEM");
                    synth.SelectVoice("Microsoft Irina Desktop");

                    synth.Rate = 1;
                    synth.Volume = 100;

                    var newFileName = Path.Combine(Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName)) +
                                      "-syn.wav";

                    synth.SetOutputToWaveFile(newFileName,
                        new SpeechAudioFormatInfo(48000, AudioBitsPerSample.Sixteen, AudioChannel.Mono));

                    synth.Speak(text);

                    return newFileName;
                }
            });

            return await task;
        }

        private static async Task<string> SynthesizeByApiAsync(string fileName, string text)
        {
            const string folderId = "b1gtgbgn9s7i2cvuc1m6"; // Укажите ID каталога.

            HttpClient client = new HttpClient();
            client.DefaultRequestHeaders.Add("Authorization", "Bearer " + iamToken);
            var values = new Dictionary<string, string>();
            values.Add("text", text);
            values.Add("lang", "ru-RU");
            values.Add("folderId", folderId);
            var content = new FormUrlEncodedContent(values);
            var response = await client.PostAsync("https://tts.api.cloud.yandex.net/speech/v1/tts:synthesize", content);
            var responseBytes = await response.Content.ReadAsByteArrayAsync();

            var newFileName = Path.Combine(Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName)) +
                              "-syn.opus";

            File.WriteAllBytes(newFileName, responseBytes);
            return newFileName;
        }

        public static byte[] ReadFully(Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                return ms.ToArray();
            }
        }

        class RecoginzeResponse
        {
            public string Result { get; set; }
        }
    }


}
