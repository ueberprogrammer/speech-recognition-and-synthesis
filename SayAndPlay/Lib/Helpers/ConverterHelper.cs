﻿using System;
using System.IO;

namespace Lib.Helpers
{
    public static class ConverterHelper
    {

        public static string WaveToOpus2(string fileName)
        {
            var newFileName = Path.GetFullPath(fileName) + Path.GetFileNameWithoutExtension(fileName) + ".opus";

            var strCmdText = $" -i {fileName} -c:a libopus -b:a 96K {newFileName}";
            System.Diagnostics.Process.Start(@"C:\audio_converter\bin\ffmpeg.exe", strCmdText);


            return newFileName;
        }

        public static string WaveToOpus(string fileName)
        {
            var process = new System.Diagnostics.Process();
            var startInfo = new System.Diagnostics.ProcessStartInfo();

            var newFileName = Path.Combine(Path.GetDirectoryName(fileName), Path.GetFileNameWithoutExtension(fileName)) + ".opus";

            startInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            startInfo.FileName = @"C:\audio_converter\bin\ffmpeg.exe";
            startInfo.Arguments = $@"-i {fileName} -c:a libopus -b:a 96K {newFileName}";
            process.StartInfo = startInfo;
            process.Start();

            process.WaitForExit();

            return newFileName;
        }
    }
}
