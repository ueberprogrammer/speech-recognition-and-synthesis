﻿using System;
using System.IO;
using System.Speech.Recognition;

namespace Lib.Helpers
{
    public static class QuickRecognizerHelper
    {
        public static bool IsSay(Stream stream, string word = "Okey")
        {
            using (SpeechRecognitionEngine recognizer =
                new SpeechRecognitionEngine(
                    new System.Globalization.CultureInfo("en-US")))
            {
                recognizer.LoadGrammar(new Grammar(new GrammarBuilder(word))); 
                
                recognizer.SetInputToWaveStream(stream);

                recognizer.InitialSilenceTimeout = TimeSpan.FromSeconds(5);

                return recognizer.Recognize() != null;
            }
        }
    }
}
