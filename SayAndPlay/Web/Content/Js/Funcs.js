﻿function drawBuffer(data) {
    var width = $("#myCanvas").width();
    var height = $("#myCanvas").height();

    var c = document.getElementById("myCanvas");
    var context = c.getContext("2d");

    var step = Math.ceil(data.length / width);
    var amp = height / 2;
    context.fillStyle = "green";
    context.clearRect(0, 0, width, height);
    for (var i = 0; i < width; i++) {
        var min = 1.0;
        var max = -1.0;
        for (j = 0; j < step; j++) {
            var datum = data[(i * step) + j];
            if (datum < min)
                min = datum;
            if (datum > max)
                max = datum;
        }
        context.fillRect(i+3, (1 + min) * amp, 1, Math.max(1, (max - min) * amp));
    }
}

function uploadBlob(blob) {

    var fd = new FormData();
    fd.append('fname', 'blob.wav');
    fd.append('data', blob);
    $.ajax({
        type: 'POST',
        url: '/v2/upload/',
        data: fd,
        processData: false,
        contentType: false
    }).done(function (data) {
        
        $("#textout").text(data.text);

        sayText(data.text);

        console.log(data.text);
    });
}
function sayText(text) {

    $.get("/v1/say/?text="+text, function (data) {
        playFile(data.file);
    });

}

function playFile(file) {
    var url = "/v1/audio/?fileName=" + file;

    var a = new Audio(encodeURI(url));

    a.volume = 1;

    a.play();
}
