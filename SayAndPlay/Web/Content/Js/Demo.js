﻿
var gumStream;
var rec; 
var input;

var counterNotZero = 0;


var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext;

function startRecording() {
    console.log("recordButton clicked");

    var constraints = { audio: true, video: false };


    navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
        console.log("getUserMedia() success, stream created, initializing Recorder.js ...");

        audioContext = new AudioContext();



        gumStream = stream;


        input = audioContext.createMediaStreamSource(stream);

        initOnAudioprocess(audioContext, input);

        rec = new Recorder(input, { numChannels: 1 });

        rec.record();

        console.log("Recording started");

    }).catch(function (err) {

        // TODO log error

    });
}

function pauseRecording() {
    console.log("pauseButton clicked rec.recording=", rec.recording);
    if (rec.recording) {
        
        rec.stop();
    } else {
        rec.record()

    }
}

function stopRecording() {
    console.log("stop recording");
    
    //tell the recorder to stop the recording
    rec.stop();

    //stop microphone access
    gumStream.getAudioTracks()[0].stop();

    //create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(createDownloadLink);

    startRecording();
}

function createDownloadLink(blob) {

    if (counterNotZero < 3) {
        counterNotZero = 0;
        return;
    }
        
    uploadBlob(blob, startRecording);

    console.log("blob: ", counterNotZero);

    counterNotZero = 0;

}

var recorder = null;
var counter = 0;
var bufferSize = 2048;
var counterZero = 0;

function initOnAudioprocess(context, mediaStream) {
    
    var numberOfInputChannels = 2;
    var numberOfOutputChannels = 1;

    if (context.createScriptProcessor) {
        recorder = context.createScriptProcessor(bufferSize,
            numberOfInputChannels,
            numberOfOutputChannels);
    } else {
        recorder = context.createJavaScriptNode(bufferSize,
            numberOfInputChannels,
            numberOfOutputChannels);
    }


    recorder.onaudioprocess = function (e) {

        var buffer = e.inputBuffer.getChannelData(0);
        var sum = 0;

        for (var i = 0; i < bufferSize; i++) {
            sum += buffer[i];
        }

        
        drawBuffer(e.inputBuffer.getChannelData(0));

        if (Math.abs(sum) < 0.9) {

            counterZero++;

            if (counterZero > 20) {
                counterZero = 0;
                recorder.onaudioprocess = null;
                stopRecording();
            }
        } else {
            counterZero = 0;
            counterNotZero++;
        }
    }

    mediaStream.connect(recorder);
    recorder.connect(context.destination);
}