﻿
URL = window.URL || window.webkitURL;

var gumStream;
var rec; 
var input;


var AudioContext = window.AudioContext || window.webkitAudioContext;
var audioContext;

var recordButton = document.getElementById("recordButton");
var stopButton = document.getElementById("stopButton");
var pauseButton = document.getElementById("pauseButton");


recordButton.addEventListener("click", startRecording);
stopButton.addEventListener("click", stopRecording);
pauseButton.addEventListener("click", pauseRecording);

function startRecording() {
    console.log("recordButton clicked");

    var constraints = { audio: true, video: false };

    recordButton.disabled = true;
    stopButton.disabled = false;
    pauseButton.disabled = false;

    navigator.mediaDevices.getUserMedia(constraints).then(function (stream) {
        console.log("getUserMedia() success, stream created, initializing Recorder.js ...");

        audioContext = new AudioContext();

        //update the format 
        document.getElementById("formats").innerHTML =
            "Format: 1 channel pcm @ " + audioContext.sampleRate / 1000 + "kHz";

        /*  assign to gumStream for later use  */
        gumStream = stream;

        /* use the stream */
        input = audioContext.createMediaStreamSource(stream);

        initOnAudioprocess(audioContext, input);

		/* 
			Create the Recorder object and configure to record mono sound (1 channel)
			Recording 2 channels  will double the file size
		*/
        rec = new Recorder(input, { numChannels: 1 });

        //start the recording process
        rec.record();

        console.log("Recording started");

    }).catch(function (err) {
        //enable the record button if getUserMedia() fails
        recordButton.disabled = false;
        stopButton.disabled = true;
        pauseButton.disabled = true
    });
}

function pauseRecording() {
    console.log("pauseButton clicked rec.recording=", rec.recording);
    if (rec.recording) {
        //pause
        rec.stop();
        pauseButton.innerHTML = "Resume";
    } else {
        //resume
        rec.record()
        pauseButton.innerHTML = "Pause";

    }
}

function stopRecording() {
    console.log("stopButton clicked");

    //disable the stop button, enable the record too allow for new recordings
    stopButton.disabled = true;
    recordButton.disabled = false;
    pauseButton.disabled = true;

    //reset button just in case the recording is stopped while paused
    pauseButton.innerHTML = "Pause";

    //tell the recorder to stop the recording
    rec.stop();

    //stop microphone access
    gumStream.getAudioTracks()[0].stop();

    //create the wav blob and pass it on to createDownloadLink
    rec.exportWAV(createDownloadLink);

    startRecording();
}

function createDownloadLink(blob) {

    $("#blobSize").text(blob.size);

    if (blob.size < 100000) {
        return;
    }

    uploadBlob(blob, startRecording);

    return;

    var url = URL.createObjectURL(blob);
    var au = document.createElement('audio');
    var li = document.createElement('li');
    var link = document.createElement('a');

    //name of .wav file to use during upload and download (without extendion)
    var filename = new Date().toISOString();

    //add controls to the <audio> element
    au.controls = true;
    au.src = url;

    //save to disk link
    link.href = url;
    link.download = filename + ".wav"; //download forces the browser to donwload the file using the  filename
    link.innerHTML = "Save to disk";

    //add the new audio element to li
    li.appendChild(au);

    //add the filename to the li
    li.appendChild(document.createTextNode(filename + ".wav "))

    //add the save to disk link to li
    li.appendChild(link);

    //upload link
    var upload = document.createElement('a');
    upload.href = "#";
    upload.innerHTML = "Upload";
    upload.addEventListener("click", function (event) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function (e) {
            if (this.readyState === 4) {
                console.log("Server returned: ", e.target.responseText);
            }
        };
        var fd = new FormData();
        fd.append("audio_data", blob, filename);
        xhr.open("POST", "upload.php", true);
        xhr.send(fd);
    })
    li.appendChild(document.createTextNode(" "))//add a space in between
    li.appendChild(upload)//add the upload link to li

    //add the li element to the ol
    recordingsList.appendChild(li);
}

var recorder = null;
var counter = 0;
//var bufferSize = 2048;
var bufferSize = 4096;
var counterZero = 0;

function initOnAudioprocess(context, mediaStream) {
    
    var numberOfInputChannels = 2;
    var numberOfOutputChannels = 1;

    if (context.createScriptProcessor) {
        recorder = context.createScriptProcessor(bufferSize,
            numberOfInputChannels,
            numberOfOutputChannels);
    } else {
        recorder = context.createJavaScriptNode(bufferSize,
            numberOfInputChannels,
            numberOfOutputChannels);
    }


    recorder.onaudioprocess = function (e) {

        var buffer = e.inputBuffer.getChannelData(0);
        var sum = 0;

        for (var i = 0; i < bufferSize; i++) {
            sum += buffer[i];
        }

        $("#output").text(buffer.length);
        drawBuffer(e.inputBuffer.getChannelData(0));

        if (Math.abs(sum) < 0.5) {

            counterZero++;

            if (counterZero > 10) {
                counterZero = 0;
                recorder.onaudioprocess = null;
                stopRecording();
            }

            //startRecording();
        } else
            counterZero = 0;

    }

    mediaStream.connect(recorder);
    recorder.connect(context.destination);
}