﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Lib.Helpers;

namespace Web.Controllers
{
    public class AudioApiController : ApiController
    {
        [HttpPost]
        [Route("v1/upload/")]
        public async Task<object> Upload()
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;

            var postedFile = httpRequest.Files[0];
            var hashNow = DateTime.Now.GetHashCode();

            var fileName = hashNow + "_" + postedFile.FileName + ".wav";

            var filePath = HttpContext.Current.Server.MapPath("~/audio/" + fileName);
            postedFile.SaveAs(filePath);

            var opusFileName = ConverterHelper.WaveToOpus(filePath);

            var text = await AudioHelper.RecognizeAsync(opusFileName);

            var filesyn = await AudioHelper.SynthesizeAsync(opusFileName, text);

            return new { text = text, file = filesyn };
        }

        [HttpPost]
        [Route("v2/upload/")]
        public async Task<object> UploadV2()
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;

            var postedFile = httpRequest.Files[0];

            var text = await AudioHelper.RecognizeV2Async(postedFile.InputStream);

            return new { text = text };
        }

        [HttpGet]
        [Route("v1/say/")]
        public async Task<object> Say(string text)
        {
            var fileName = HttpContext.Current.Server.MapPath("~/audio/" + Guid.NewGuid() + ".wav");


            var newFileName = await AudioHelper.SynthesizeAsync(fileName, text);

            return new { file = newFileName };
        }

        [Route("v1/audio/"), HttpGet]
        public HttpResponseMessage AudioAsync(string fileName)
        {
            const string audioDir = @"C:\repository\speech-recognition-and-synthesis\SayAndPlay\Web\Audio";

            var data = File.ReadAllBytes(Path.Combine(audioDir, fileName));

            var memStream = new MemoryStream(data);

            var mediaTypeHeaderValue = new MediaTypeHeaderValue("audio/mpeg");
            if (this.Request.Headers.Range != null)
            {
                try
                {
                    var partialResponse = this.Request.CreateResponse(HttpStatusCode.PartialContent);
                    partialResponse.Content =
                        new ByteRangeStreamContent(memStream, this.Request.Headers.Range, mediaTypeHeaderValue);

                    return partialResponse;
                }
                catch (InvalidByteRangeException invalidByteRangeException)
                {
                    return this.Request.CreateErrorResponse(invalidByteRangeException);
                }
            }

            var fullResponse = this.Request.CreateResponse(HttpStatusCode.OK);
            fullResponse.Content = new StreamContent(memStream);
            fullResponse.Content.Headers.ContentType = mediaTypeHeaderValue;

            return fullResponse;
        }

        [Route("v1/is-okey/"), HttpPost]
        public bool IsOkey()
        {
            HttpResponseMessage result = null;
            var httpRequest = HttpContext.Current.Request;

            var postedFile = httpRequest.Files[0];

            return QuickRecognizerHelper.IsSay(postedFile.InputStream, "Okay");
        }
    }
}