﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Record()
        {
            return View();
        }

        public ActionResult Dynamic()
        {
            return View();
        }

        public ActionResult Demo()
        {
            return View();
        }
    }
}